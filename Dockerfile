FROM python:3.10-slim

LABEL \
    maintainer="Vladimir Markov <Markovvn1@gmail.com>" \
    description="Yet Another Code Sharing Project"

ENV \
    TERM=xterm-256color \
    \
    # Don't periodically check PyPI to determine whether a new version of pip is available for download.
    PIP_DISABLE_PIP_VERSION_CHECK=on \
    # Disable package cache.
    PIP_NO_CACHE_DIR=on \
    # Python won’t try to write .pyc files on the import of source modules.
    PYTHONDONTWRITEBYTECODE=on \
    # install a handler for SIGSEGV, SIGFPE, SIGABRT, SIGBUS and SIGILL signals to dump the Python traceback
    PYTHONFAULTHANDLER=on \
    # Force the stdout and stderr streams to be unbuffered.
    PYTHONUNBUFFERED=on \
    # set workdir as PYTHONPATH
    PYTHONPATH=/opt/app

STOPSIGNAL SIGINT

WORKDIR /opt/app

COPY pyproject.toml poetry.lock /opt/app

# Install poetry
RUN set -ex \
    && pip install poetry==1.3.2 \
    && poetry config virtualenvs.create false  \
    && poetry install --only main \
    && rm -rf /root/.cache/*

RUN set -ex \
    && apt-get -qq update \
    && apt-get -qq install wget \
    && rm -rf /var/lib/apt/lists/* \
    # Create non-root user
    && useradd -s /bin/bash python

USER python

COPY . /opt/app

EXPOSE 80

HEALTHCHECK --interval=5s --timeout=10s CMD bash -c "[ \"`wget -T 1 --no-verbose -O - http://localhost/healthy`\" == \"ok\" ] || exit 1"

ENTRYPOINT ["uvicorn", "--host", "0.0.0.0", "--port", "80", "--proxy-headers", "--forwarded-allow-ips", "*", "yacsp.main:app"]
