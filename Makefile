VENV ?= .venv
CODE = yacsp tests scripts

.PHONY: venv
venv:
	python -m venv $(VENV)
	$(VENV)/bin/python -m pip install --upgrade pip
	$(VENV)/bin/python -m pip install poetry
	$(VENV)/bin/poetry install

.PHONY: lint
lint:
	# check package version
	$(VENV)/bin/python -c "import yacsp as a; exit(a.__version__ != \"`poetry version -s`\")"
	$(VENV)/bin/poetry lock --check
	# python linters
	$(VENV)/bin/pflake8 --jobs 4 --statistics --show-source $(CODE)
	$(VENV)/bin/pylint --recursive=y --jobs 4 $(CODE)
	$(VENV)/bin/mypy $(CODE)
	$(VENV)/bin/black --skip-string-normalization --check $(CODE)

.PHONY: format
format:
	$(VENV)/bin/isort $(CODE)
	$(VENV)/bin/black $(CODE)
	$(VENV)/bin/pautoflake --recursive --remove-all-unused-imports --in-place $(CODE)

.PHONY: test
test:
	$(VENV)/bin/pytest -v tests

.PHONY: up
up:
	$(VENV)/bin/uvicorn yacsp.main:app --reload

