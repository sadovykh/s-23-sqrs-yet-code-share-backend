import secrets

import pytest

from yacsp.services.file_uid import BadFileUID, FileUIDKey, FileUIDService


@pytest.fixture(name="file_uid_service")
def _file_uid_service() -> FileUIDService:
    return FileUIDService(FileUIDService._generate_secret_key())


def test_key_generate() -> None:
    key = FileUIDService._generate_secret_key()
    assert isinstance(key, FileUIDKey)
    assert len(key.perm) == 6 * 8
    assert len(key.local) == 256
    assert all(len(lkey.perm) == 5 * 8 for lkey in key.local)


def test_key_encode_decode() -> None:
    key1 = FileUIDService._generate_secret_key()
    key2 = FileUIDService._generate_secret_key()
    assert key1 != key2
    key1_enc = FileUIDService._encode_key(key1)
    assert isinstance(key1_enc, str)
    key1_denc = FileUIDService._decode_key(key1_enc)
    assert key1 == key1_denc


@pytest.mark.parametrize("_id", [secrets.randbits(8 if i < 8 else 32) for i in range(16)])
def test_id2uid(_id: int, file_uid_service: FileUIDService) -> None:
    uid = file_uid_service.id2uid(_id)
    assert isinstance(uid, str) and len(uid) == 8
    assert file_uid_service.uid2id(uid) == _id


@pytest.mark.parametrize("uid", ["ui_😋jkgg", "abcde", "dfe-_/\\d", "jhkjsddy"])
def test_id2uid_wrong_uid(uid: str, file_uid_service: FileUIDService) -> None:
    with pytest.raises(BadFileUID):
        file_uid_service.uid2id(uid)


@pytest.mark.parametrize("_id", [secrets.randbits(8 if i < 8 else 32) for i in range(16)])
def test_id2uid_wrong_key(_id: int, file_uid_service: FileUIDService) -> None:
    uid = file_uid_service.id2uid(_id)
    file_uid_service._secret_key = FileUIDService._generate_secret_key()  # use new key
    with pytest.raises(BadFileUID):
        file_uid_service.uid2id(uid)
