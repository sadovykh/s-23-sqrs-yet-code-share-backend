import secrets

import pytest

from yacsp.services.jwt import BadTokenJWTServiceException, JWTService


@pytest.fixture(name="jwt_service")
def _jwt_service() -> JWTService:
    return JWTService(JWTService._generate_secret_key())


@pytest.mark.parametrize("user_id", [secrets.randbits(8 if i < 4 else 32) for i in range(8)])
def test_jwt_token(user_id: int, jwt_service: JWTService) -> None:
    token = jwt_service.create_access_token_for_user(user_id)
    assert isinstance(token, str)
    assert jwt_service.get_user_id_from_token(token) == user_id


@pytest.mark.parametrize("token", ["ui_😋jkgg", "abcde", "dfe-_/\\d", "jhkjsddy"])
def test_jwt_wrong_token(token: str, jwt_service: JWTService) -> None:
    with pytest.raises(BadTokenJWTServiceException):
        jwt_service.get_user_id_from_token(token)


@pytest.mark.parametrize("user_id", [secrets.randbits(8 if i < 4 else 32) for i in range(8)])
def test_jwt_wrong_key(user_id: int, jwt_service: JWTService) -> None:
    token = jwt_service.create_access_token_for_user(user_id)
    jwt_service._secret_key = JWTService._generate_secret_key()  # use new key
    with pytest.raises(BadTokenJWTServiceException):
        jwt_service.get_user_id_from_token(token)
