from fastapi.testclient import TestClient

from yacsp.main import app

client = TestClient(app)


def test_healthy() -> None:
    response = client.get("/healthy")
    assert response.status_code == 200
    assert response.json() == "ok"
