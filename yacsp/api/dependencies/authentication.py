from fastapi import Depends, HTTPException, Request
from fastapi.security import HTTPAuthorizationCredentials, HTTPBearer
from starlette.status import HTTP_401_UNAUTHORIZED, HTTP_503_SERVICE_UNAVAILABLE

from yacsp.api.dependencies.services import get_service
from yacsp.services.google_recaptcha2 import GoogleRecaptcha2, GoogleRecaptcha2Exception
from yacsp.services.jwt import BadTokenJWTServiceException, JWTService

bearer_scheme = HTTPBearer()


def get_current_user_id(
    credentials: HTTPAuthorizationCredentials = Depends(bearer_scheme),
    jwt: JWTService = Depends(get_service(JWTService)),
) -> int:
    if credentials.scheme != "Bearer":
        raise HTTPException(
            status_code=HTTP_401_UNAUTHORIZED,
            detail="Invalid authentication scheme",
            headers={"WWW-Authenticate": "Bearer"},
        )

    try:
        return jwt.get_user_id_from_token(credentials.credentials)
    except BadTokenJWTServiceException as e:
        raise HTTPException(
            status_code=HTTP_401_UNAUTHORIZED,
            detail="Invalid or expired token",
        ) from e


async def verifyed_with_captcha(
    captcha_token: str,
    request: Request,
    recaptcha: GoogleRecaptcha2 = Depends(get_service(GoogleRecaptcha2)),
) -> None:
    ip_address = request.client.host if request.client is not None else ""
    try:
        if not await recaptcha.verify(captcha_token, ip_address):
            raise HTTPException(
                status_code=HTTP_401_UNAUTHORIZED,
                detail="Incorrect recaptcha token",
            )
    except GoogleRecaptcha2Exception as e:
        raise HTTPException(
            status_code=HTTP_503_SERVICE_UNAVAILABLE,
            detail="Failed to check recaptcha token",
        ) from e
