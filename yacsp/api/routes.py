# pylint: disable=unused-argument


from fastapi import APIRouter, Depends, HTTPException, Request
from starlette.status import HTTP_200_OK, HTTP_201_CREATED, HTTP_400_BAD_REQUEST, HTTP_404_NOT_FOUND

from yacsp.db.exceptions import NotFoundRepoException
from yacsp.db.repositories import FilesRepository, LanguageRepository, MarksRepository, UsersRepository
from yacsp.models import (
    FileInResponse,
    Language,
    NewFileCreatedResponse,
    NewFileInRequest,
    NewMarkRequest,
    UserMark,
    UserTokenInResponse,
)
from yacsp.services import FileUIDService, JWTService
from yacsp.services.file_uid import BadFileUID

from .dependencies import get_current_user_id, get_repository, get_service, verifyed_with_captcha

router = APIRouter()


@router.get(
    "/user/register",
    response_model=UserTokenInResponse,
    status_code=HTTP_200_OK,
    summary="Get user using its ip address or create a new user",
    dependencies=[Depends(verifyed_with_captcha)],
)
async def register_new_user(
    request: Request,
    jwt: JWTService = Depends(get_service(JWTService)),
    user_repo: UsersRepository = Depends(get_repository(UsersRepository)),
) -> UserTokenInResponse:
    ip_address = request.client.host if request.client is not None else ""
    user_id = user_repo.create_user(ip_address)
    token = jwt.create_access_token_for_user(user_id)
    return UserTokenInResponse(access_token=token, token_type="bearer")


@router.get("/languages", response_model=list[Language], summary="Get all supported languages")
def get_languages(language_repo: LanguageRepository = Depends(get_repository(LanguageRepository))) -> list[Language]:
    return language_repo.get_all()


@router.post(
    "/file/new",
    response_model=NewFileCreatedResponse,
    status_code=HTTP_201_CREATED,
    summary="Create new file",
)
def new_file(
    body: NewFileInRequest,
    user_id: int = Depends(get_current_user_id),
    files_repo: FilesRepository = Depends(get_repository(FilesRepository)),
    file_uid_service: FileUIDService = Depends(get_service(FileUIDService)),
) -> NewFileCreatedResponse:
    try:
        file_id = files_repo.save_file(body)
    except NotFoundRepoException as exc:
        raise HTTPException(status_code=HTTP_400_BAD_REQUEST, detail="Incorrect input data") from exc
    return NewFileCreatedResponse(uid=file_uid_service.id2uid(file_id))


@router.get("/file/{file_uid}", response_model=FileInResponse, status_code=HTTP_200_OK, summary="Get file by its uid")
def get_file(
    file_uid: str,
    user_id: int = Depends(get_current_user_id),
    files_repo: FilesRepository = Depends(get_repository(FilesRepository)),
    file_uid_service: FileUIDService = Depends(get_service(FileUIDService)),
) -> FileInResponse:
    try:
        file_id = file_uid_service.uid2id(file_uid)
    except BadFileUID as exc:
        raise HTTPException(status_code=HTTP_400_BAD_REQUEST, detail="Incorrect file uid") from exc
    try:
        res = files_repo.get_file_for_response(file_id=file_id, file_uid=file_uid, user_id=user_id)
    except NotFoundRepoException as exc:
        raise HTTPException(status_code=HTTP_404_NOT_FOUND, detail="File not found") from exc
    return res


@router.put("/file/{file_uid}/mark", status_code=HTTP_200_OK, summary="Change user's mark for this file")
def update_file_mark(
    body: NewMarkRequest,
    user_id: int = Depends(get_current_user_id),
    mark_repo: MarksRepository = Depends(get_repository(MarksRepository)),
    file_repo: FilesRepository = Depends(get_repository(FilesRepository)),
    file_uid_service: FileUIDService = Depends(get_service(FileUIDService)),
) -> None:
    try:
        file_id = file_uid_service.uid2id(body.file_uid)
    except BadFileUID as exc:
        raise HTTPException(status_code=HTTP_400_BAD_REQUEST, detail="Incorrect file uid") from exc
    try:
        file_repo.get_file_for_response(file_id=file_id, file_uid=body.file_uid, user_id=user_id)
    except NotFoundRepoException as exc:
        raise HTTPException(status_code=HTTP_404_NOT_FOUND, detail="File not found") from exc
    mark_repo.set(UserMark(user_id=user_id, file_id=file_id, mark=body.new_mark))


@router.get("/top", response_model=list[FileInResponse], status_code=HTTP_200_OK, summary="Get top files by stars")
def get_file_top(
    offset: int,
    limit: int,
    user_id: int = Depends(get_current_user_id),
    file_repo: FilesRepository = Depends(get_repository(FilesRepository)),
    file_uid_service: FileUIDService = Depends(get_service(FileUIDService)),
) -> list[FileInResponse]:
    files = file_repo.get_top_files(user_id=user_id, offset=offset, limit=limit)
    return [
        FileInResponse(
            uid=file_uid_service.id2uid(i.id),
            title=i.title,
            content=i.content,
            language=i.language,
            visibility=i.visibility,
            mark=i.mark,
            star_count=i.star_count,
        )
        for i in files
    ]
