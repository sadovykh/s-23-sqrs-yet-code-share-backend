class RepoException(Exception):
    pass


class AlreadyExistRepoException(RepoException):
    pass


class NotFoundRepoException(RepoException):
    pass
