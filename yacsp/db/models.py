from sqlalchemy import BLOB, Column, DateTime, ForeignKey, Integer, String
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import relationship
from sqlalchemy.sql import functions

SQLBase = declarative_base()


class ParamsDB(SQLBase):
    __tablename__ = "params"
    name = Column(String, unique=True, primary_key=True)
    value = Column(String)


class UserDB(SQLBase):
    __tablename__ = "user"
    user_id = Column(Integer, primary_key=True)

    remote_ip_address = Column(String(64))
    created_at = Column(DateTime, server_default=functions.now())
    files = relationship("FileDB", secondary="mark", back_populates="users")


class LanguageDB(SQLBase):
    __tablename__ = "language"
    language_id = Column(Integer, primary_key=True)
    name = Column(String(64), unique=True)


class FileDB(SQLBase):
    __tablename__ = "files"
    file_id = Column(Integer, primary_key=True)

    title = Column(String, nullable=False)
    content = Column(BLOB, nullable=False)
    language_id = Column(Integer, ForeignKey("language.language_id"), nullable=False)
    language = relationship("LanguageDB")
    visibility = Column(Integer, nullable=False)
    created_at = Column(DateTime, server_default=functions.now())
    users = relationship("UserDB", secondary="mark", back_populates="files")


class MarkDB(SQLBase):
    __tablename__ = "mark"
    user_id = Column(Integer, ForeignKey("user.user_id"), primary_key=True)
    file_id = Column(Integer, ForeignKey("files.file_id"), primary_key=True)
    mark = Column(Integer, nullable=False)
