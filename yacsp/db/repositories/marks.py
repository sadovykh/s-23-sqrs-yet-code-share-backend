from yacsp.core import AppEvent
from yacsp.db.models import MarkDB
from yacsp.models import UserMark

from .base import BaseRepository


class MarksRepository(BaseRepository, AppEvent):
    def set(self, user_mark: UserMark) -> None:
        mark_db = MarkDB(user_id=user_mark.user_id, file_id=user_mark.file_id, mark=user_mark.mark.id)
        self._sess.merge(mark_db)
        self._sess.commit()
