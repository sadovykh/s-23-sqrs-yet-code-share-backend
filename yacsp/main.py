import asyncio
from typing import Awaitable, Callable

from fastapi import FastAPI
from fastapi.middleware.cors import CORSMiddleware
from loguru import logger
from prometheus_fastapi_instrumentator import Instrumentator

from .api import router
from .core import AppEvent
from .project import VERSION
from .settings import Settings


def create_start_app_handler(_app: FastAPI) -> Callable[[], Awaitable[None]]:
    async def start_app() -> None:
        logger.info("Application is starting")
        await asyncio.gather(*[i.init() for i in AppEvent.__subclasses__()])

    return start_app


def create_stop_app_handler(_app: FastAPI) -> Callable[[], Awaitable[None]]:
    async def stop_app() -> None:
        logger.info("Application is stopping")
        await asyncio.gather(*[i.deinit() for i in AppEvent.__subclasses__()])

    return stop_app


def get_application() -> FastAPI:
    res = FastAPI(title="YACSP API", version=VERSION)
    if Settings.get_instance().allow_all_cors:
        res.add_middleware(
            CORSMiddleware,
            allow_origins=["*"],
            allow_credentials=True,
            allow_methods=["*"],
            allow_headers=["*"],
        )
    res.add_event_handler("startup", create_start_app_handler(res))
    res.add_event_handler("shutdown", create_stop_app_handler(res))
    res.include_router(router, prefix="/api")
    return res


app = get_application()
Instrumentator().instrument(app).expose(app)


@app.get("/healthy")
async def healthy() -> str:
    return "ok"
