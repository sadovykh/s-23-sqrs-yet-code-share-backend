from pydantic import BaseModel

from yacsp.utils.str_enum_with_id import StrEnumWithId


class UserTokenInResponse(BaseModel):
    access_token: str
    token_type: str


class FileVisibility(StrEnumWithId):
    PRIVATE = "private", 0
    PUBLIC = "public", 1


class FileMark(StrEnumWithId):
    NO_STAR = "no_star", 0
    STAR = "star", 1


class Language(BaseModel):
    id: int
    name: str


class FileInResponse(BaseModel):
    uid: str
    title: str
    content: str
    language: Language
    visibility: FileVisibility
    mark: FileMark
    star_count: int


class FileInResponseWithId(BaseModel):
    id: int
    title: str
    content: str
    language: Language
    visibility: FileVisibility
    mark: FileMark
    star_count: int


class NewFileInRequest(BaseModel):
    title: str
    content: str
    language_id: int
    visibility: FileVisibility


class NewFileCreatedResponse(BaseModel):
    uid: str


class UserMark(BaseModel):
    user_id: int
    file_id: int
    mark: FileMark


class NewMarkRequest(BaseModel):
    file_uid: str
    new_mark: FileMark
