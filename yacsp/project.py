import pathlib

VERSION = "0.1.0"

CURRENT_PATH = pathlib.Path().absolute()
PROJECT_PATH = pathlib.Path(__file__).absolute().parent
STORAGE_PATH = CURRENT_PATH / "storage"
