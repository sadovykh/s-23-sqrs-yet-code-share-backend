from typing import Optional

import aiohttp

from yacsp.core import AppEvent
from yacsp.settings import Settings

from .base import BaseService


class GoogleRecaptcha2Exception(Exception):
    pass


class FailedToVerify(GoogleRecaptcha2Exception):
    pass


class GoogleRecaptcha2(BaseService, AppEvent):
    RECAPTCHA2_VERIFY_URL = "https://www.google.com/recaptcha/api/siteverify"
    __instance: Optional["GoogleRecaptcha2"] = None

    _client: aiohttp.ClientSession
    _recaptcha_secret_key: str

    def __init__(self, recaptcha_secret_key: str) -> None:
        self._recaptcha_secret_key = recaptcha_secret_key
        timeout = aiohttp.ClientTimeout(total=10)
        connector = aiohttp.TCPConnector(limit_per_host=30)
        self._client = aiohttp.ClientSession(timeout=timeout, connector=connector)

    async def close(self) -> None:
        await self._client.close()

    @classmethod
    async def init(cls) -> None:
        cls.get_instance()

    @classmethod
    async def deinit(cls) -> None:
        if cls.__instance is not None:
            await cls.get_instance().close()
            cls.__instance = None

    @classmethod
    def get_instance(cls) -> "GoogleRecaptcha2":
        if cls.__instance is None:
            cls.__instance = cls(Settings.get_instance().google_recaptcha_server_token)
        return cls.__instance

    async def verify(self, token: str, remote_addr: str) -> bool:
        payload = {
            "secret": self._recaptcha_secret_key,
            "response": token,
            "remoteip": remote_addr,
        }
        try:
            async with self._client.post(self.RECAPTCHA2_VERIFY_URL, data=payload) as responce:
                if responce.status != 200:
                    raise FailedToVerify(f"Incorrect responce status (got {responce.status}, expected 200)")
                if not isinstance(res := (await responce.json()).get("success"), bool):
                    raise FailedToVerify("Incorrect server responce")
                return res
        except Exception as e:
            raise FailedToVerify() from e
