import secrets
from datetime import datetime, timedelta
from typing import Any, Dict, Optional

from jose import JWTError, jwt
from loguru import logger
from pydantic import BaseModel, ValidationError

from yacsp.core import AppEvent
from yacsp.db.exceptions import NotFoundRepoException
from yacsp.db.repositories import ParamsRepository

from .base import BaseService


class JWTData(BaseModel):
    exp: datetime
    sub: str


class JWTServiceException(Exception):
    pass


class BadTokenJWTServiceException(JWTServiceException):
    pass


class JWTService(BaseService, AppEvent):
    DEFAULT_EXPIRES_DELTA = timedelta(days=30)
    JWT_ALGORITHM = "HS256"

    __instance: Optional["JWTService"] = None

    _secret_key: str

    def __init__(self, secret_key: str):
        self._secret_key = secret_key

    @staticmethod
    def _generate_secret_key() -> str:
        return secrets.token_hex(32)

    @classmethod
    def _get_or_generate_secret_key(cls, params_repo: ParamsRepository) -> str:
        try:
            res = params_repo.get("jwt_secret_key")
            logger.info("JWT secret key loaded")
            return res
        except NotFoundRepoException:
            res = cls._generate_secret_key()
            params_repo.set("jwt_secret_key", res)
            logger.info("New JWT secret key gererated")
            return res

    @classmethod
    async def init(cls) -> None:
        cls.get_instance()

    @classmethod
    async def deinit(cls) -> None:
        if cls.__instance is not None:
            cls.__instance = None

    @classmethod
    def get_instance(cls) -> "JWTService":
        if cls.__instance is None:
            with ParamsRepository.create() as params_repo:
                secret_key = cls._get_or_generate_secret_key(params_repo)
            cls.__instance = cls(secret_key)
        return cls.__instance

    def _create_jwt_token(
        self,
        subject: str,
        expires_delta: timedelta,
        payload: Optional[Dict[str, Any]] = None,
    ) -> str:
        to_encode = (payload or {}).copy()
        to_encode.update(JWTData(sub=subject, exp=datetime.utcnow() + expires_delta).dict())
        return jwt.encode(to_encode, self._secret_key, algorithm=self.JWT_ALGORITHM)

    def create_access_token_for_user(self, user_id: int, expires_delta: Optional[timedelta] = None) -> str:
        return self._create_jwt_token(str(user_id), expires_delta or self.DEFAULT_EXPIRES_DELTA)

    def get_user_id_from_token(self, token: str) -> int:
        try:
            payload = jwt.decode(token, self._secret_key, algorithms=[self.JWT_ALGORITHM])
        except JWTError as e:
            raise BadTokenJWTServiceException("Failed for decode and verify given token") from e

        try:
            data = JWTData(**payload)
        except ValidationError as e:
            raise BadTokenJWTServiceException("Malformed payload in token") from e

        try:
            return int(data.sub)
        except ValueError as e:
            raise BadTokenJWTServiceException("malformed payload in token: sub is not int") from e
