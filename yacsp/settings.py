from typing import Optional

from pydantic import BaseSettings


class Settings:
    __instance: Optional["SettingsEnv"] = None

    class SettingsEnv(BaseSettings):
        google_recaptcha_server_token: str = ""
        allow_all_cors: bool = False

    @classmethod
    def _init(cls, instance: SettingsEnv) -> None:
        cls.__instance = instance

    @classmethod
    def get_instance(cls) -> "SettingsEnv":
        if cls.__instance is None:
            cls.__instance = cls.SettingsEnv()
        return cls.__instance
